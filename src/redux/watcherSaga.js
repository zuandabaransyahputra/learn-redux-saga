import { all } from "redux-saga/effects";
import userSagas from "../modules/user/userSagas";

export function* watcherSaga() {
    yield all([...userSagas])
}
