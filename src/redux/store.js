import { createStore, combineReducers, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { watcherSaga } from './watcherSaga';
import { userReducer } from '../modules/user/userReducers';


//Tempat seluruh reducer
const reducer = combineReducers({
    user: userReducer
})

const sagaMiddleware = createSagaMiddleware()

const middleware = [sagaMiddleware];

const store = createStore(
    reducer,
    composeWithDevTools(applyMiddleware(...middleware))
)

sagaMiddleware.run(watcherSaga)

export default store