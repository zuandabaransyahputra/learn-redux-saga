import * as CONST from './userConstants'

export const getUser = () => ({
    type: CONST.GET_USER_REQUEST
})

export const getUserSucess = (payload) => ({
    type: CONST.GET_USER_SUCCESS,
    payload
})

export const getUserFailed = (payload) => ({
    type: CONST.GET_USER_FAIL,
    payload
})
