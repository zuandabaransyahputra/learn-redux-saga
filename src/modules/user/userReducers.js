import * as CONST from './userConstants';
import * as STATE from './userInitialState';

const initialState = {
  ...STATE.userInitialState,
  action: '',
};

export const userReducer = (state = initialState, action) => {
  const { type, payload } = action;
  const actions = {
    [CONST.GET_USER_REQUEST]: () => ({
      ...state,
      loading: true,
      action: type,
    }),
    [CONST.GET_USER_SUCCESS]: () => ({
      ...state,
      loading: false,
      user: payload,
      action: type,
    }),
    [CONST.GET_USER_FAIL]: () => ({
      ...state,
      loading: false,
      error: payload,
      action: type,
    }),
    DEFAULT: () => state,
  };
  return (actions[type] || actions.DEFAULT)();
};
