import axios from 'axios'

export const getUserAPI = () => {
    return axios.get('https://jsonplaceholder.typicode.com/users')
}