import { call, takeLatest, put } from "redux-saga/effects";
import { getUserSucess, getUserFailed } from "./userActions";
import { getUserAPI } from './userApis'
import { GET_USER_REQUEST } from "./userConstants";

function* getUserSaga() {
    try {
        const response = yield call(getUserAPI)
        if (response) {
            switch (response.status) {
                case 200:
                    yield put(getUserSucess(response.data))
                    break;
                case 400:
                    yield put(getUserFailed(response.data))
                    break;
                default:
            }
        }
    } catch (error) {
        yield put(getUserFailed(error.response))
    }
}

export default [takeLatest(GET_USER_REQUEST, getUserSaga)]