//Initial state untuk user -> agar lebih paham bisa dilihat di redux devtools
//di redux devtools terdapat initial usernya, yang nantinya akan berubah sesuai dengan type actionnya
export const userInitialState = {
    loading: false,
    user: undefined,
    error: ''
}