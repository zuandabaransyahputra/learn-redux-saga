import { useEffect } from 'react';
import { connect } from 'react-redux';
import { getUser as getUserProps } from './modules/user/userActions'
import Loader from './components/Loader'

function App(props) {
  const { user, getUser, loading } = props

  useEffect(() => {
    //Meminta data ke action
    getUser()
  }, [getUser])

  return (
    <div className="App">
      {loading && <Loader />}
      {user && user.map(i => (
        <p key={i.id}>{i.name} <br /></p>
      ))}
    </div>
  );
}

//Mengambil state dari store.js -> bisa dilihat di redux devtools
const mapStateToProps = (state) => ({
  user: state.user.user,
  loading: state.user.loading
})

//Mengambil function getUser dari action dan meminta data user ke action
const mapDispatchToProps = {
  getUser: () => getUserProps(),
};

export default connect(mapStateToProps, mapDispatchToProps)(App)

